# Les 10 méthodes vues

# puts
# gets
# chomp
# to_i
# to_f
# to_s
# +
# -
# *
# /


# puts 'Salut ' .+ 'les gars'
# puts (10.* 9).+ 9

# les méthodes orginales de chaines

# var1 = 'stop'
# var2 = 'stressed'
# var3 = 'pouvez-vous prononcer ceci inversé ?'
# puts var1.reverse
# puts var2.reverse
# puts var3.reverse
# puts var1
# puts var2
# puts var3


#
puts 'Quel est votre nom complet ?'
nom = gets.chomp
puts 'Savez-vous qu\' il y a ' + nom.length + ' caractères dans votre nom, ' + nom + ' ?'