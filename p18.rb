# Les méthodes gets et chomp

# puts gets

# puts 'Bonjour, quel est donc votre nom ?'
# monnom = gets.chomp
# puts 'Votre nom est ' + monnom + '? Quel joli nom !'
# puts 'Ravi de faire votre connaissance ' + monnom +  '. ;)'


# EXERCICE 1
# Ecrire un programme qui demande à une personne son premier prénom, 
# son deuxième, et enfin son nom de famille. 
# En sortie le programme devra féliciter la personne en utilisant son identité complète.

# puts 'Quel est donc ton prénom ?'
# prenom = gets.chomp
# puts 'Ensuite ton nom de famille'
# nom = gets.chomp
# puts 'Bien joué, t\'es un bon ptit gars ' + prenom + ' ' + nom + '.'


# EXERCICE 2
# Ecrire un programme qui demande à une personne son nombre favori. 
# Votre programme devra ajouter un à ce nombre, et proposer le résultat 
# comme étant plus grand et donc meilleur pour un favori
# (Dites-le tout de même avec tact !).

puts 'Quel est votre nombre favori ?'
var = gets.chomp
var = var.to_i
myvar = var + 1
puts 'votre favori est ' + var.to_s + ' alors que le mien est le ' + myvar.to_s
puts 'Désolé mais j\'ai gagné' 