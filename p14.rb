# 3. Variables et affectations

# Variables
maChaine = '...pouvez-vous répéter...'
puts maChaine
puts maChaine

# Affectation
nom = 'Hubert de La PâteFeuilletée'
puts 'Mon nom est ' + nom + '.'
puts 'Ouf ' + nom + 'est vraimnent un nom trop long'


# ré affectation
compositeur = 'Mozart'
puts compositeur + ' était "une vedette", en son temps.'
compositeur = 'Beethoven'
puts 'Mais je préfère ' + compositeur + ', personnellement'


var = 'juste une autre ' + 'chaîne'
puts var
var = 5 * (1 + 2)
puts var


var1 = 8
var2 = var1
puts var1
puts var2
puts ''
var1 = 'Huit'
puts var1
puts var2